# Docker cmd sift r 

## What is a container 

A container is a unit of software that contains code (including the features of host OS required) and all of the code dependancies so that the application can run quickly and reliably from one comuting environment to another.

Dependecies include:

- sytem tools
- libaries
- settings

Containers are a lighter-weight, more agile way of handling virtualization — since they don't use a hypervisor, you can enjoy faster resource provisioning and speedier availability of new applications. 

Rather than spinning up an entire virtual machine, containerization packages together everything needed to run a single application or microservice (along with runtime libraries they need to run). The container includes all the code, its dependencies and even the operating system itself. This enables applications to run almost anywhere — a desktop computer, a traditional IT infrastructure or the cloud.

Containers use a form of operating system (OS) virtualization. Put simply, they leverage features of the host operating system to isolate processes and control the processes’ access to CPUs, memory and desk space.

Containers have been around for decades, but the common consensus is that the modern container era began in 2013 with the introduction of Docker, an open source platform for building, deploying and managing containerized applications. Learn more about Docker, Docker containers, Dockerfiles (the container image's build file) and how the ecosystem has evolved with container technology over the last decade.

## How is it different to a VM

Conclusion Virtual machines and containers differ in several ways, but the primary difference is that containers provide a way to virtualize an OS so that multiple workloads can run on a single OS instance. With VMs, the hardware is being virtualized to run multiple OS instances

n traditional virtualization, a hypervisor virtualizes physical hardware. The result is that each virtual machine contains a guest OS, a virtual copy of the hardware that the OS requires to run and an application and its associated libraries and dependencies. VMs with different operating systems can be run on the same physical server. For example, a VMware VM can run next to a Linux VM, which runs next to a Microsoft VM, etc.

Instead of virtualizing the underlying hardware, containers virtualize the operating system (typically Linux or Windows) so each individual container contains only the application and its libraries and dependencies. Containers are small, fast, and portable because, unlike a virtual machine, containers do not need to include a guest OS in every instance and can, instead, simply leverage the features and resources of the host OS. 

Just like virtual machines, containers allow developers to improve CPU and memory utilization of physical machines. Containers go even further, however, because they also enable microservice architectures, where application components can be deployed and scaled more granularly. This is an attractive alternative to having to scale up an entire monolithic application because a single component is struggling with load.

## How is it similar to a VM



## What is docker?

A subset of the Moby project. It is a software framework for building, runninf and managing containers on servers and the cloud.

Docker is a set of platform as a service products that use OS-level virtualization to deliver software in packages called containers.



## What other services of Containers exist?

Thanks to the OCI (Open Container Initiative) there is a wide range to chose from without getting locked in.

Other services that exist:

- CRI-O
- Podman
- LXC

## Main sections of docker

Dockers main sections consit of:

- Docker Swarm
- Docker compose
- Docker images 
- Docker Daemon 
- Docker Engine

Dockers three main components are:

- Server (The docker daemon called dockerd, can create and manage new docker images)
- Rest AIP (It is useed to instrust daemon what to do)
- CLI (a client used to enter docker commands)




## DOCKER COMMANDS

docker is teh containerization tool 

It pull images form dockerhub

Docker hub hosts many images! Like nginx or httpd

main commands
```bash
# Start a container using docker run
$ docker run -d -p 80:80 docker/getting started 

# -d is for detached mode
# -p is for port mapping [inside_container]:[outside_container]

# Check containers running 
$ docker ps

# Stop a container 
$ docker stop 

# Docker images to see images in computer 
$ docker images

# 
docker run

docker build 

```

go get an image of httpd 
how can you "ssh" into your docker container 
have a look around 

look at options `-dit ` what does that do?
 
oesaiufhoafj