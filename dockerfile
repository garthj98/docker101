# We want a Redhat-Centos httpd server

# Get a base container with REDhat-Centos installed
FROM centos:centos7.9.2009 

# Run a command to install httpd
RUN yum -y install httpd

# Add our own content 
COPY index.html /var/www/html

# Explicitly mark port 80 as exposed - more for kubernates and docker compose that needs to know explicitly where servers are running 
EXPOSE 80

# A command that will run everytime a container is launched that starts httpd
# Entry point like user data for AMI
ENTRYPOINT ["httpd", "-DFOREGROUND"]